<?php
    require_once('script/dbcon.php');

?>
<!DOCTYPE html>
<html>

<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>แก้ไขคอร์ส</title>

	<link href="img/favicon.144x144.png" rel="apple-touch-icon" type="image/png" sizes="144x144">
	<link href="img/favicon.114x114.png" rel="apple-touch-icon" type="image/png" sizes="114x114">
	<link href="img/favicon.72x72.png" rel="apple-touch-icon" type="image/png" sizes="72x72">
	<link href="img/favicon.57x57.png" rel="apple-touch-icon" type="image/png">
	<link href="img/favicon.png" rel="icon" type="image/png">
	<link href="img/favicon.ico" rel="shortcut icon">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="css/separate/vendor/select2.min.css">
	<link rel="stylesheet" href="css/lib/font-awesome/font-awesome.min.css">
	<link rel="stylesheet" href="css/separate/vendor/bootstrap-touchspin.min.css">
	<link rel="stylesheet" href="css/lib/font-awesome/font-awesome.min.css">
	<link rel="stylesheet" href="css/lib/bootstrap/bootstrap.min.css">
	<link rel="stylesheet" href="css/main.css">
</head>

<body class="with-side-menu">

	<header class="site-header">
		<div class="container-fluid">
			<a href="#" class="site-logo">
				<img class="hidden-md-down" src="img/logoo.jpg" alt="">
			</a>


			<div class="site-header-content">
				<div class="site-header-content-in">
					<div class="site-header-shown">


						<div class="dropdown user-menu">
							<button class="dropdown-toggle" id="dd-user-menu" type="button" data-toggle="dropdown"
								aria-haspopup="true" aria-expanded="false">
								<img src="img/avatar-2-64.png" alt="">
							</button>
							<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dd-user-menu">
								<a class="dropdown-item" href="#"><span
										class="font-icon glyphicon glyphicon-user"></span>โปรไฟล์</a>


								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="script/logout.php"><span
										class="font-icon glyphicon glyphicon-log-out"></span>Logout</a>
							</div>
						</div>

						<button type="button" class="burger-right">
							<i class="font-icon-menu-addl"></i>
						</button>
					</div>
					<!--.site-header-shown-->

					<div class="mobile-menu-right-overlay"></div>
					<div class="site-header-collapsed">
						<div class="site-header-collapsed-in">

							<div class="dropdown dropdown-typical">
								<a class="dropdown-toggle no-arr" href="index_admin.php">

									<span class="lbl">หน้าแรก</span>
								</a>
							</div>

							<div class="dropdown dropdown-typical">
								<a class="dropdown-toggle no-arr" href="#">

									<span class="lbl">เกี่ยวกับ</span>
								</a>

							</div>
							<div class="dropdown dropdown-typical">
								<a href="#" class="dropdown-toggle no-arr">

									<span class="lbl">โปรโมชั่น</span>
								</a>
							</div>

							<div class="dropdown dropdown-typical">
								<a class="dropdown-toggle no-arr" href="course_admin.php">

									<span class="lbl">คอร์ส</span>
								</a>
							</div>

							<div class="dropdown dropdown-typical">
								<a class="dropdown-toggle no-arr" href="#">

									<span class="lbl">กิจกรรม</span>
								</a>
							</div>

							<div class="dropdown dropdown-typical">
								<a class="dropdown-toggle no-arr" href="#">

									<span class="lbl">ข้อมูลโค้ช</span>
								</a>
							</div>

							<div class="dropdown dropdown-typical">
								<a class="dropdown-toggle no-arr" href="#">

									<span class="lbl">ติดต่อ</span>
								</a>
							</div>



						</div>
						<!--.site-header-collapsed-in-->
					</div>
					<!--.site-header-collapsed-->
				</div>
				<!--site-header-content-in-->
			</div>
			<!--.site-header-content-->
		</div>
		<!--.container-fluid-->
	</header>
	<!--.site-header-->



	<div class="mobile-menu-left-overlay"></div>


	<div class="page-content">
		<div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3>แก้ไขคอร์ส</h3>

						</div>
					</div>
				</div>
			</header>

			<div class="box-typical box-typical-padding">

				<?php 

					$sql = "SELECT * FROM `product` where pid =". $_GET['pid'];
					$result = $con->query($sql);

					if ($result->num_rows > 0) {
  					while($row = $result->fetch_assoc()) { 
				?>

				<form action="script/action_editcourse.php" method="post" enctype="multipart/form-data">
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">ชื่อคอร์ส</label>
						<div class="col-sm-10">
							<input type="text" name="inputname" class="form-control" id="inputname"
								placeholder="ชื่อคอร์ส" value="<?php echo $row["name"] ?>">
							<input type="hidden" name="id" class="form-control" placeholder="รหัสคอร์ส"
								value=" <?php echo $row["pid"] ?>">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">ราคา</label>
						<div class="col-sm-10">
							<input type="text" name="price" class="form-control" placeholder="ราคา"
								value="<?php echo $row["price"] ?>">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">ระยะเวลา</label>
						<div class="col-sm-10">
							<input type="text" name="time" class="form-control" placeholder="ระยะเวลา"
								value="<?php echo $row["time"] ?>">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 form-control-label"></label>
						<div class="col-sm-10">
							<img id="output_img" src="<?php echo $row["img"] ?>" width="500px" />
							<script>
								var loadFile = function (event) {
									var output = document.getElementById('output_img');
									output.src = URL.createObjectURL(event.target.files[0]);
									output.onload = function () {
										URL.revokeObjectURL(output.src) // free memory
									}
								};
							</script>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-sm-2 form-control-label">รูปภาพ</label>
						<div class="col-sm-10">
							<input type="file" name="fileToUpload" id="fileToUpload" onchange="loadFile(event)">
						</div>
					</div>

					<div class="form-group row">
						<label for="exampleSelect" class="col-sm-2 form-control-label">รายละเอียด</label>
						<div class="col-sm-10">
							<textarea rows="4" class="form-control" name="Description"
								id="Description"><?php echo $row["description"] ?></textarea>
						</div>
					</div>
					<div class="form-group row">

						<div class="col-sm-offset-2 col-sm-10">
							<input type="submit" name="submit" class="btn btn-success" value="แก้ไข">
							<a href="index_admin.php" class="btn btn-primary" role="button">กลับหน้าแรก</a>
						</div>

					</div>

					<?php
  						}
						}
					?>
				</form>

			</div>
			<!--.box-typical-->
		</div>
		<!--.container-fluid-->
	</div>
	<!--.page-content-->

	<script src="js/lib/jquery/jquery-3.2.1.min.js"></script>
	<script src="js/lib/popper/popper.min.js"></script>
	<script src="js/lib/tether/tether.min.js"></script>
	<script src="js/lib/bootstrap/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>

	<script src="js/lib/select2/select2.full.min.js"></script>
	<script src="js/lib/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>
	<script>
		$(document).ready(function () {
			$("input[name='demo1']").TouchSpin({
				min: 0,
				max: 100,
				step: 0.1,
				decimals: 2,
				boostat: 5,
				maxboostedstep: 10,
				postfix: '%'
			});
		});
	</script>
	<script>
		$(document).ready(function () {
			$("input[name='demo2']").TouchSpin({
				min: -1000000000,
				max: 1000000000,
				stepinterval: 50,
				maxboostedstep: 10000000,
				prefix: '$'
			});
		});
	</script>
	<script>
		$(document).ready(function () {
			$("input[name='demo_vertical']").TouchSpin({
				verticalbuttons: true
			});
		});
	</script>
	<script>
		$(document).ready(function () {
			$("input[name='demo_vertical2']").TouchSpin({
				verticalbuttons: true,
				verticalupclass: 'glyphicon glyphicon-plus',
				verticaldownclass: 'glyphicon glyphicon-minus'
			});
		});
	</script>
	<script>
		$(document).ready(function () {
			$("input[name='demo3']").TouchSpin();
		});
	</script>
	<script>
		$(document).ready(function () {
			$("input[name='demo4']").TouchSpin({
				postfix: "a button",
				postfix_extraclass: "btn btn-default"
			});
		});
	</script>
	<script>
		$(document).ready(function () {
			$("input[name='demo4_2']").TouchSpin({
				postfix: "a button",
				postfix_extraclass: "btn btn-default"
			});
		});
	</script>
	<script>
		$(document).ready(function () {
			$("input[name='demo6']").TouchSpin({
				buttondown_class: "btn btn-default-outline",
				buttonup_class: "btn btn-default-outline"
			});
		});
	</script>
	<script>
		$(document).ready(function () {
			$("input[name='demo5']").TouchSpin({
				prefix: "pre",
				postfix: "post"
			});
		});
	</script>

	<script src="js/app.js"></script>
</body>

</html>