<?php
	session_start();
	require_once('script/dbcon.php');
	
	if (isset( $_GET['p_id'])) {
		$p_id = $_GET['p_id']; 
		$act = $_GET['act'];
	
		if($act=='add' && !empty($p_id))
		{
			$_SESSION['cart'][$p_id]=1;
		}

		if($act=='remove' && !empty($p_id))  //ยกเลิกการสั่งซื้อ
		{
			unset($_SESSION['cart'][$p_id]);
            header('location: cart.php');

		}
		
	
	}
	// echo "<pre>";  
	// print_r($_SESSION['cart']);
	// echo "</pre>";
	$CheckDurationTime = 0;
	if(!empty($_SESSION['cart'])){
		$tmp_arry = array();
	
		foreach($_SESSION['cart'] as $ID => $p_id){ 
								
			$sql = "SELECT * FROM cousrse WHERE pid ='" . $ID . "'";
			$result = $con->query($sql);
			$Courses = mysqli_fetch_array($result);
			
			$sql2 = "SELECT * FROM duration_cousrse WHERE pid ='" . $ID . "'";
			$result2 = $con->query($sql2);
			foreach ($result2 as $key2 => $value2) {     
				array_push($tmp_arry,$value2);
			}
		}

		foreach($tmp_arry as $key=>$val1){
			foreach($tmp_arry as $key2=>$val2){
				if($key !== $key2){
					// $date1 = new DateTime($val1['date_cousrse']);
					// $date2 = new DateTime($val2["date_cousrse"]); 
				
					if($val1['date_cousrse'] == $val2["date_cousrse"]) {
						if( strtotime($val1['time_start']) >= strtotime($val2['time_start'])  || strtotime($val1['time_start']) <= strtotime($val2['time_end'])){
							// echo "<pre>";
							// print_r($val1);
							// print_r($val2);
							// print_r('-------------------');
							// echo "</pre>";
							$CheckDurationTime = 1;
							$_SESSION['CheckDurationTime'] = "มีวันและเวลาในคอร์สชนกัน ไม่สามารถจองได้ .!";
							break;
						}
						
					}
		
				}
					
			}
		}
	}
?>


<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Course Cart</title>

	<link href="img/favicon.144x144.png" rel="apple-touch-icon" type="image/png" sizes="144x144">
	<link href="img/favicon.114x114.png" rel="apple-touch-icon" type="image/png" sizes="114x114">
	<link href="img/favicon.72x72.png" rel="apple-touch-icon" type="image/png" sizes="72x72">
	<link href="img/favicon.57x57.png" rel="apple-touch-icon" type="image/png">
	<link href="img/favicon.png" rel="icon" type="image/png">
	<link href="img/favicon.ico" rel="shortcut icon">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="css/lib/lobipanel/lobipanel.min.css">
	<link rel="stylesheet" href="css/separate/vendor/lobipanel.min.css">
	<link rel="stylesheet" href="css/lib/jqueryui/jquery-ui.min.css">
	<link rel="stylesheet" href="css/separate/pages/widgets.min.css">
    <link rel="stylesheet" href="css/lib/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="css/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/separate/pages/prices.min.css">
    <link rel="stylesheet" href="css/lib/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="css/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">
</head>
<body class="with-side-menu control-panel control-panel-compact">

	
	<?php include 'header.php';?>

	
	<div class="page-content">
	    <div class="container">
			<div class="card">
				<div class="card-header">
						ตะกร้าสินค้า
				</div>
				<div class="card-body">
						<?php if (isset($_SESSION['CheckDurationTime'])) : ?>
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <strong>
                                    <?php 
                                            echo $_SESSION['CheckDurationTime'];
                                            unset($_SESSION['CheckDurationTime']);
                                        ?>
                                </strong> 
                            </div>                       
                        <?php endif ?>
						<table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col" style="width: 10%;">#</th>
                                        <th scope="col" style="width: 20%; text-align: left;">ชื่อคอร์ส</th>
										<th scope="col" style="width: 20%; text-align: center;">ราคาคอร์ส</th>
                                        <th scope="col" style="width: 20%; text-align: center;">ราคาหลังหักส่วนลดโปรโมชั่น</th>
                                        <th scope="col" style="width: 10%;">ลบคอร์ส</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
									$total=0;

									if(!empty($_SESSION['cart'])){
									
										$key = 1;
										$Today = date("Y-m-d");
										foreach($_SESSION['cart'] as $ID => $p_id){ 
										
										$sql = "SELECT * FROM cousrse WHERE pid ='" . $ID . "'";
										$result = $con->query($sql);
										$Courses = mysqli_fetch_array($result);
										$sql2 = "SELECT * FROM promotion  WHERE rid ='" . $Courses['rid'] . "'";
										$result2 = $con->query($sql2);
										$Promotions = mysqli_fetch_array($result2);
										if(isset($Promotions) && ($Today < $Promotions["end_time"])){
											$total += ($Courses['price'] - $Promotions['price']);
										}
										else{
											$total += ($Courses['price']);
										}
										
                                    ?>
                                    <tr>
									
                                        <th scope="row"> <?php echo  $key ?></th>
                                        <td  style="text-align: left;"> <?php echo  $Courses['name'] ?> </td>
										<td  style="text-align: center;"> <?php echo  $Courses['price'] ?> </td>
                                        <td  style="text-align: center;"> 
											<?php 
												if(isset($Promotions) && ($Today < $Promotions["end_time"])){
													echo  $Courses['price'] - $Promotions['price'];
												}
												else{
													echo $Courses['price'];
												}
											?> 
										</td>
										<td> <a class="btn btn-danger" href='cart.php?p_id=<?php echo $Courses['pid']; ?>&act=remove'>ลบ</a> </td>

                                    </tr>
                                    <?php
									$key++;
                                        }
                                    }
                                    ?>
                                </tbody>
								<?php if(!empty($_SESSION['cart'])){  ?>
								<tfoot >
									<tr class="table-primary">
										<th colspan='3' style="text-align: center;">ราคารวม</th>
										<th style="text-align: center;"> <b> <?php echo number_format($total,2)  ?>  </b></th>
										<th > </th>
									</tr>
									<tr >
									<th ><a href="course.php">กลับหน้าคอร์ส</a></th>
									<th colspan='4' style="text-align: right;"> 
										<?php if ( $CheckDurationTime == 1 ) { ?>
											<button type="button" class="btn btn-danger" disabled>ไม่สามารถจองได้</button>

										<?php }else{ ?>
											<a class="btn btn-success" href="confirm_course.php" >ยืนยันการจอง</a>
										<?php } ?>
										
									</th>
									</tr>
								</tfoot>
								<?php } ?>
                            </table>
				</div>
			</div>
	    </div><!--.container-fluid-->
	</div><!--.page-content-->



	<script src="js/lib/jquery/jquery-3.2.1.min.js"></script>
	<script src="js/lib/popper/popper.min.js"></script>
	<script src="js/lib/tether/tether.min.js"></script>
	<script src="js/lib/bootstrap/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>

	<script type="text/javascript" src="js/lib/jqueryui/jquery-ui.min.js"></script>
	<script type="text/javascript" src="js/lib/lobipanel/lobipanel.min.js"></script>
	<script type="text/javascript" src="js/lib/match-height/jquery.matchHeight.min.js"></script>
	

<script src="js/app.js"></script>
</body>
</html>