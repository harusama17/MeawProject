<?php 
    session_start();
?>
<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>จองคอร์ส</title>

	<link href="img/favicon.144x144.png" rel="apple-touch-icon" type="image/png" sizes="144x144">
	<link href="img/favicon.114x114.png" rel="apple-touch-icon" type="image/png" sizes="114x114">
	<link href="img/favicon.72x72.png" rel="apple-touch-icon" type="image/png" sizes="72x72">
	<link href="img/favicon.57x57.png" rel="apple-touch-icon" type="image/png">
	<link href="img/favicon.png" rel="icon" type="image/png">
	<link href="img/favicon.ico" rel="shortcut icon">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

<link rel="stylesheet" href="css/separate/vendor/select2.min.css">
<link rel="stylesheet" href="css/lib/font-awesome/font-awesome.min.css">
<link rel="stylesheet" href="css/separate/vendor/bootstrap-touchspin.min.css">
    <link rel="stylesheet" href="css/lib/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="css/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">

</head>
<body class="with-side-menu">
	
	<?php include 'header.php';?>

	
	

	<div class="mobile-menu-left-overlay"></div>

    <?php  require_once('script/dbcon.php');?>
    <?php 
		$HourCousrse = 0;
		$MinuteCousrse = 0;
        $sql = "SELECT * FROM cousrse WHERE pid ='" . $_GET['pid'] . "'";
		
        $result = $con->query($sql);
        $data = mysqli_fetch_array($result);
		// JOIN promotion ON cousrse.rid = promotion.rid
    ?>
	<div class="page-content">
		<div class="container">
	
			<div class="card mb-3">
				<!-- <div class="card-header">
					<?php echo $data["name"]; ?>
				</div> -->
				<div class="card-body">
					<img src="<?php echo  $data["img"] ?>" class="rounded mx-auto d-block" width="400" height="400">

					<h2 class="card-title"> ชื่อคอร์ส : <?php echo $data["name"]; ?> </h2>
					<h5 class="card-text"> รายละเอียดคอร์ส : <?php echo $data["description"]; ?>  </h5>
					<h5 class="card-text"> ราคาคอร์ส : <?php echo $data["price"]; ?>   </h5>
					<?php if (isset($data['rid'])) { 
					  $CheckEndPromotion = false;
					  $sqlPromo = "SELECT * FROM promotion  WHERE rid ='" . $data['rid'] . "'";
					  $resultPromo = $con->query($sqlPromo);
					  $Promotions = mysqli_fetch_array($resultPromo);

						$sql2 = "SELECT * FROM duration_cousrse WHERE pid ='" . $_GET['pid'] . "'";
						$result2 = $con->query($sql2);
						if ($result2->num_rows > 0) {
							foreach ($result2 as $key2 => $value2) {     
								$HourCousrse += $value2['hours'];
								$MinuteCousrse += $value2['minutes'];
							}
						}

						$Today = date("Y-m-d");
	
						if ($Today > $Promotions["end_time"]){
							$CheckEndPromotion  = true;
						}
					?>

					<?php if($CheckEndPromotion ) { ?>
						<h5 class="card-text" style="text-decoration:line-through"> ราคารวมส่วนลดโปรโมชั่นเหลือเพียง : <span style="color: red;" > สิ้นสุดโปรโมชั่น </span>  </h5>

					<?php }else{  ?>
						<h5 class="card-text"> ราคารวมส่วนลดโปรโมชั่นเหลือเพียง : <span class="badge bg-danger">  <?php echo $data["price"] - $Promotions["price"]; ?>  </span>  </h5>

					<?php } ?>
					

					<?php }else{  ?>
						<h5 class="card-text"> ราคารวมส่วนลดโปรโมชั่นเหลือเพียง : <span class="badge bg-danger"> ไม่มีโปรโมชั่นเข้าร่วม  </span>  </h5>
					<?php } ?>
					<h5 class="card-text"> ระยะเวลาเข้าเรียน :  
											<?php $date = new DateTime($data['start_date']); echo $date->format('d-m-Y') ?>    
											- 
											<?php $date = new DateTime($data['end_date']); echo $date->format('d-m-Y') ?>    
					</h5>
					<h5 class="card-text"> ชั่วโมงการเข้าเรียนคอร์ส : <?php echo $HourCousrse; ?> ชั่วโมง <?php echo $MinuteCousrse; ?> นาที  </h5>

					<hr/>
					<h2 class="card-text"> วันเข้าเรียนคอร์ส  </h2>
					<ul class="list-group">

						<?php 
							if ($result2->num_rows > 0) {
								foreach ($result2 as $key2 => $value2) {  
						?>
							<li class="list-group-item d-flex justify-content-between align-items-center">
								วันเข้าเรียน : <?php  
								switch ($value2['date_cousrse']) {
										case 1:
										echo "วันจันทร์";
										break;
										case 2:
										echo "วันอังคาร";
										break;
										case 3:
										echo "วันพุธ";
										break;
										case 4:
										echo "วันพฤหัสบดี";
										break;
										case 5:
										echo "วันศุกร์";
										break;
										case 6:
										echo "วันเสาร์";
										break;
										case 7:
										echo "วันอาทิตย์";
										break;
									} ?>
								<span class="card-text"> <?php  echo $value2['time_start']  ?> - <?php  echo $value2['time_end']  ?> </span>
							</li>
						<?php
                                }
                            }
                        ?>
					</ul>
	

					<div class="row" style="padding-top: 2rem; padding-bottom: 2rem; padding-left: 30rem;">
                        <a href="cart.php?p_id=<?php echo  $data["pid"] ?>&act=add" name="insert" class="btn btn-primary" style="margin-right: 1rem;">เพิ่มลงตะกร้าสินค้า</a>
                        <a href="course.php" class="btn btn-danger">ย้อนกลับ</a>
                               
                    </div>
				</div>
			</div>

		</div><!--.container-fluid-->
	</div><!--.page-content-->

	<script src="js/lib/jquery/jquery-3.2.1.min.js"></script>
	<script src="js/lib/popper/popper.min.js"></script>
	<script src="js/lib/tether/tether.min.js"></script>
	<script src="js/lib/bootstrap/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>

	<script src="js/lib/select2/select2.full.min.js"></script>
	<script src="js/lib/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>
	<script>
		$(document).ready(function() {
			$("input[name='demo1']").TouchSpin({
				min: 0,
				max: 100,
				step: 0.1,
				decimals: 2,
				boostat: 5,
				maxboostedstep: 10,
				postfix: '%'
			});
		});
	</script>
	<script>
		$(document).ready(function(){
			$("input[name='demo2']").TouchSpin({
				min: -1000000000,
				max: 1000000000,
				stepinterval: 50,
				maxboostedstep: 10000000,
				prefix: '$'
			});
		});
	</script>
	<script>
		$(document).ready(function(){
			$("input[name='demo_vertical']").TouchSpin({
				verticalbuttons: true
			});
		});
	</script>
	<script>
		$(document).ready(function(){
			$("input[name='demo_vertical2']").TouchSpin({
				verticalbuttons: true,
				verticalupclass: 'glyphicon glyphicon-plus',
				verticaldownclass: 'glyphicon glyphicon-minus'
			});
		});
	</script>
	<script>
		$(document).ready(function(){
			$("input[name='demo3']").TouchSpin();
		});
	</script>
	<script>
		$(document).ready(function(){
			$("input[name='demo4']").TouchSpin({
				postfix: "a button",
				postfix_extraclass: "btn btn-default"
			});
		});
	</script>
	<script>
		$(document).ready(function(){
			$("input[name='demo4_2']").TouchSpin({
				postfix: "a button",
				postfix_extraclass: "btn btn-default"
			});
		});
	</script>
	<script>
		$(document).ready(function(){
			$("input[name='demo6']").TouchSpin({
				buttondown_class: "btn btn-default-outline",
				buttonup_class: "btn btn-default-outline"
			});
		});
	</script>
	<script>
		$(document).ready(function(){
			$("input[name='demo5']").TouchSpin({
				prefix: "pre",
				postfix: "post"
			});
		});
	</script>

<script src="js/app.js"></script>
</body>
</html>