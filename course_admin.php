<?php
    require_once('script/dbcon.php');

    ?>
<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>คอร์สแอดมิน</title>

	<link href="img/favicon.144x144.png" rel="apple-touch-icon" type="image/png" sizes="144x144">
	<link href="img/favicon.114x114.png" rel="apple-touch-icon" type="image/png" sizes="114x114">
	<link href="img/favicon.72x72.png" rel="apple-touch-icon" type="image/png" sizes="72x72">
	<link href="img/favicon.57x57.png" rel="apple-touch-icon" type="image/png">
	<link href="img/favicon.png" rel="icon" type="image/png">
	<link href="img/favicon.ico" rel="shortcut icon">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="css/lib/lobipanel/lobipanel.min.css">
	<link rel="stylesheet" href="css/separate/vendor/lobipanel.min.css">
	<link rel="stylesheet" href="css/lib/jqueryui/jquery-ui.min.css">
	<link rel="stylesheet" href="css/separate/pages/widgets.min.css">
    <link rel="stylesheet" href="css/lib/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="css/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/separate/pages/prices.min.css">
    <link rel="stylesheet" href="css/lib/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="css/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">
</head>
<body class="with-side-menu control-panel control-panel-compact">

		<header class="site-header">
	    <div class="container-fluid">
	        <a href="#" class="site-logo">
	            <img class="hidden-md-down" src="img/logoo.jpg" alt="">
	        </a>
	
	        
	        <div class="site-header-content">
	            <div class="site-header-content-in">
	                <div class="site-header-shown">
	                    
	                   
	                    <div class="dropdown user-menu">
	                        <button class="dropdown-toggle" id="dd-user-menu" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	                            <img src="img/avatar-2-64.png" alt="">
	                        </button>
	                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dd-user-menu">
	                            <a class="dropdown-item" href="#"><span class="font-icon glyphicon glyphicon-user"></span>โปรไฟล์</a>
	                            
	                            
	                            <div class="dropdown-divider"></div>
	                            <a class="dropdown-item" href="script/logout.php"><span class="font-icon glyphicon glyphicon-log-out"></span>Logout</a>
	                        </div>
	                    </div>
	
	                    <button type="button" class="burger-right">
	                        <i class="font-icon-menu-addl"></i>
	                    </button>
	                </div><!--.site-header-shown-->
	
	                <div class="mobile-menu-right-overlay"></div>
	                <div class="site-header-collapsed">
	                    <div class="site-header-collapsed-in">
	                       
	                        <div class="dropdown dropdown-typical">
	                            <a class="dropdown-toggle no-arr" href="index_admin.php" >
	                                
	                                <span class="lbl">หน้าแรก</span>
	                            </a>
	                        </div>

	                        <div class="dropdown dropdown-typical">
	                            <a class="dropdown-toggle no-arr" href="#" >
	                              
	                                <span class="lbl">เกี่ยวกับ</span>
	                            </a>
	
	                        </div>
	                        <div class="dropdown dropdown-typical">
	                            <a href="#" class="dropdown-toggle no-arr">
	                              
	                                <span class="lbl">โปรโมชั่น</span>
	                            </a>
	                        </div>
	
	                        <div class="dropdown dropdown-typical">
	                            <a class="dropdown-toggle no-arr" href="course_admin.php" >
	                                
	                                <span class="lbl">คอร์ส</span>
	                            </a>
	                        </div>

	                        <div class="dropdown dropdown-typical">
	                            <a class="dropdown-toggle no-arr" href="#" >
	                               
	                                <span class="lbl">กิจกรรม</span>
	                            </a>
	                        </div>

	                        <div class="dropdown dropdown-typical">
	                            <a class="dropdown-toggle no-arr" href="#" >
	                                
	                                <span class="lbl">ข้อมูลโค้ช</span>
	                            </a>
	                        </div>

	                        <div class="dropdown dropdown-typical">
	                            <a class="dropdown-toggle no-arr" href="#" >
	                               
	                                <span class="lbl">ติดต่อ</span>
	                            </a>
	                        </div>
	                        
	                       
	                        
	                    </div><!--.site-header-collapsed-in-->
	                </div><!--.site-header-collapsed-->
	            </div><!--site-header-content-in-->
	        </div><!--.site-header-content-->
	    </div><!--.container-fluid-->
	</header><!--.site-header-->
	

	<div class="page-content">
	    <div class="container-fluid">
	        <section class="box-typical box-typical-full-height">
				<div class="box-typical-center">
					<div class="box-typical-center-in prices-page">
						<header class="prices-page-title">คอร์สเรียนทั้งหมด</header>
						<p class="prices-page-subtitle">ด่วน!!! จำนวนจำกัด รีบจองได้เลย</p>
						
						<?php 

						$sql = "SELECT * FROM `product`";
						$result = $con->query($sql);

						if ($result->num_rows > 0) {
  						// output data of each row
  						while($row = $result->fetch_assoc()) { ?>


  							<article class="price-card">
							<header class="price-card-header"> <?php echo  $row["name"] ?> </header>
							<div class="price-card-body">
								<img src="<?php echo  $row["img"] ?>" width="200" height="200">
								
								<ul class="price-card-list">
									<li><i class="font-icon font-icon-ok"></i><?php echo  $row["description"] ?></li>
									<li><i class="font-icon font-icon-ok"></i><?php echo  $row["time"] ?></li>
									<li><i class="font-icon font-icon-ok"></i>ราคา <?php echo  $row["price"] ?> บาท</li>
								</ul>
								<div class="clear"></div>
								<a href="editcourse.php?pid=<?php echo $row["pid"] ?>" class="btn btn-rounded">แก้ไข</a>
								<a onclick="deletePost(<?php echo $row['pid'] ?>)" class="btn btn-rounded"color="#FFFFFF" >ลบคอร์ส</a>
									<script>
									function deletePost(p_id){
									    console.log("delete = "+p_id);
									     $.post("script/deletecourse.php",{"delete": p_id+""},function(data, status){
									    location.reload();
									  });
									}
									
									</script>
							</div>
						</article>

						<?php
  						}
						}
						?>
						
						<article class="price-card" style="vertical-align: top;margin-top: 20%;">
						
							<div class="price-card-body">
								<div class="clear"></div>
								<a href="addcourse.php" class="btn btn-rounded">เพิ่มคอร์ส </a>
							</div>

						</article>
						
						
					</div>
				</div>
			</section><!--.box-typical.prices-page-->
	
	    </div><!--.container-fluid-->
	</div><!--.page-content-->



	<script src="js/lib/jquery/jquery-3.2.1.min.js"></script>
	<script src="js/lib/popper/popper.min.js"></script>
	<script src="js/lib/tether/tether.min.js"></script>
	<script src="js/lib/bootstrap/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>

	<script type="text/javascript" src="js/lib/jqueryui/jquery-ui.min.js"></script>
	<script type="text/javascript" src="js/lib/lobipanel/lobipanel.min.js"></script>
	<script type="text/javascript" src="js/lib/match-height/jquery.matchHeight.min.js"></script>
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<script>
		$(document).ready(function() {
            $('.panel').each(function () {
                try {
                    $(this).lobiPanel({
                        sortable: true
                    }).on('dragged.lobiPanel', function(ev, lobiPanel){
                        $('.dahsboard-column').matchHeight();
                    });
                } catch (err) {}
            });

			google.charts.load('current', {'packages':['corechart']});
			google.charts.setOnLoadCallback(drawChart);
			function drawChart() {
				var dataTable = new google.visualization.DataTable();
				dataTable.addColumn('string', 'Day');
				dataTable.addColumn('number', 'Values');
				// A column for custom tooltip content
				dataTable.addColumn({type: 'string', role: 'tooltip', 'p': {'html': true}});
				dataTable.addRows([
					['MON',  130, ' '],
					['TUE',  130, '130'],
					['WED',  180, '180'],
					['THU',  175, '175'],
					['FRI',  200, '200'],
					['SAT',  170, '170'],
					['SUN',  250, '250'],
					['MON',  220, '220'],
					['TUE',  220, ' ']
				]);

				var options = {
					height: 314,
					legend: 'none',
					areaOpacity: 0.18,
					axisTitlesPosition: 'out',
					hAxis: {
						title: '',
						textStyle: {
							color: '#fff',
							fontName: 'Proxima Nova',
							fontSize: 11,
							bold: true,
							italic: false
						},
						textPosition: 'out'
					},
					vAxis: {
						minValue: 0,
						textPosition: 'out',
						textStyle: {
							color: '#fff',
							fontName: 'Proxima Nova',
							fontSize: 11,
							bold: true,
							italic: false
						},
						baselineColor: '#16b4fc',
						ticks: [0,25,50,75,100,125,150,175,200,225,250,275,300,325,350],
						gridlines: {
							color: '#1ba0fc',
							count: 15
						}
					},
					lineWidth: 2,
					colors: ['#fff'],
					curveType: 'function',
					pointSize: 5,
					pointShapeType: 'circle',
					pointFillColor: '#f00',
					backgroundColor: {
						fill: '#008ffb',
						strokeWidth: 0,
					},
					chartArea:{
						left:0,
						top:0,
						width:'100%',
						height:'100%'
					},
					fontSize: 11,
					fontName: 'Proxima Nova',
					tooltip: {
						trigger: 'selection',
						isHtml: true
					}
				};

				var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
				chart.draw(dataTable, options);
			}
			$(window).resize(function(){
				drawChart();
				setTimeout(function(){
				}, 1000);
			});
		});
	</script>

<script src="js/app.js"></script>
</body>
</html>