<?php


$DATABASE_HOST = 'localhost';
$DATABASE_USER = 'root';
$DATABASE_PASS = '';
$DATABASE_NAME = 'database_academy';

$con = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);
// set character set utf-8
mysqli_query($con, 'SET CHARACTER SET UTF8');

if ( mysqli_connect_errno() ) {

	die ('Failed to connect to database: ' . mysqli_connect_error());
}
?>