<?php
	session_start();
	require_once('script/dbcon.php');
	
    if(empty($_SESSION['cart'])){ 
        header('location: index.php');
    }
?>

<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Course Cart</title>

	<link href="img/favicon.144x144.png" rel="apple-touch-icon" type="image/png" sizes="144x144">
	<link href="img/favicon.114x114.png" rel="apple-touch-icon" type="image/png" sizes="114x114">
	<link href="img/favicon.72x72.png" rel="apple-touch-icon" type="image/png" sizes="72x72">
	<link href="img/favicon.57x57.png" rel="apple-touch-icon" type="image/png">
	<link href="img/favicon.png" rel="icon" type="image/png">
	<link href="img/favicon.ico" rel="shortcut icon">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="css/lib/lobipanel/lobipanel.min.css">
	<link rel="stylesheet" href="css/separate/vendor/lobipanel.min.css">
	<link rel="stylesheet" href="css/lib/jqueryui/jquery-ui.min.css">
	<link rel="stylesheet" href="css/separate/pages/widgets.min.css">
    <link rel="stylesheet" href="css/lib/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="css/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/separate/pages/prices.min.css">
    <link rel="stylesheet" href="css/lib/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="css/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">
</head>
<body class="with-side-menu control-panel control-panel-compact">

	
	<?php include 'header.php';?>

    
	<div class="page-content">
	    <div class="container">
			<div class="card">
				<div class="card-header">
						ยืนยันคอร์ส
				</div>
				<div class="card-body">
						<table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col" style="width: 10%;">#</th>
                                        <th scope="col" style="width: 20%; text-align: left;">ชื่อคอร์ส</th>
										<th scope="col" style="width: 20%; text-align: center;">ราคาคอร์ส</th>
                                        <th scope="col" style="width: 20%; text-align: center;">ราคาหลังหักส่วนลดโปรโมชั่น</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
									$total=0;

									if(!empty($_SESSION['cart'])){
									
										$key = 1;
										$Today = date("Y-m-d");
										foreach($_SESSION['cart'] as $ID => $p_id){ 
										
										$sql = "SELECT * FROM cousrse WHERE pid ='" . $ID . "'";
										$result = $con->query($sql);
										$Courses = mysqli_fetch_array($result);
										$sql2 = "SELECT * FROM promotion  WHERE rid ='" . $Courses['rid'] . "'";
										$result2 = $con->query($sql2);
										$Promotions = mysqli_fetch_array($result2);
										if(isset($Promotions) && ($Today < $Promotions["end_time"])){
											$total += ($Courses['price'] - $Promotions['price']);
										}
										else{
											$total += ($Courses['price']);
										}
                                    ?>
                                    <tr>
									
                                        <th scope="row"> <?php echo  $key ?></th>
                                        <td  style="text-align: left;"> <?php echo  $Courses['name'] ?> </td>
										<td  style="text-align: center;"> <?php echo  $Courses['price'] ?> </td>
										<td  style="text-align: center;"> 
											<?php 
												if(isset($Promotions) && ($Today < $Promotions["end_time"])){
													echo  $Courses['price'] - $Promotions['price'];
												}
												else{
													echo $Courses['price'];
												}
											?> 
										</td>
                                    </tr>
                                    <?php
									$key++;
                                        }
                                    }
                                    ?>
                                </tbody>
								<tfoot >
									<tr class="table-primary">
										<th colspan='2' style="text-align: center;">ราคารวม</th>
										<th style="text-align: center;"> <b> <?php echo number_format($total,2)  ?>  </b></th>
										<th > </th>
									</tr>
								</tfoot>
							
                        </table>
				</div>
			</div>

            <div class="box-typical box-typical-padding">
                <h3>รายละเอียดในการติดต่อ</h3>                   
				<form action="script/action_bookcourse.php" method="post" enctype="multipart/form-data">

					<div class="form-group row">
						<label class="col-sm-2 form-control-label">ชื่อ-นามสกุล : </label>
						<div class="col-sm-8">
							<p class="form-control-static"><input type="text" name ="name" class="form-control" placeholder="กรอกชื่อและนามสกุล" required></p>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-sm-2 form-control-label">ที่อยู่</label>
						<div class="col-sm-8">
							<p class="form-control-static"><input type="text" name ="address" class="form-control" id="address" placeholder="กรอกที่อยู่" required></p>
						</div>
					</div>
					
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">เบอร์โทรศัพท์</label>
						<div class="col-sm-8">
							<p class="form-control-static"><input type="text" name ="phone" class="form-control" id="phone" placeholder="กรอกเบอร์โทรศัพท์" maxlength="10" minlength="10" required></p>
						</div>
					</div>
					
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">อีเมล</label>
						<div class="col-sm-8">
							<p class="form-control-static"><input type="email" name ="email" class="form-control" id="email" placeholder="กรอกอีเมล" required></p>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-sm-2 form-control-label">ไอดีไลน์</label>
						<div class="col-sm-8">
							<p class="form-control-static"><input type="text" name ="lineid" class="form-control" id="lineid" placeholder="กรอกไอดีไลน์"></p>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-sm-2 form-control-label">แนบสลิปเงินมัดจำ</label>
						<div class="col-sm-8">
                            <input type="file" class="form-control" name="fileToUpload" id="fileToUpload" accept="image/*">
						</div>
					</div>

					<div class="form-group row">
						
						<div class="col-sm-offset-4 col-sm-10">
                            <input type="submit" name="submit" class="btn btn-success" value="ยืนยันข้อมูล">
                            <a href="cart.php" class="btn btn-danger" role="button">ย้อนกลับ</a>
                        </div>

					</div>
				</form>

				
			</div><!--.box-typical-->
	    </div><!--.container-fluid-->
	</div><!--.page-content-->



	<script src="js/lib/jquery/jquery-3.2.1.min.js"></script>
	<script src="js/lib/popper/popper.min.js"></script>
	<script src="js/lib/tether/tether.min.js"></script>
	<script src="js/lib/bootstrap/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>

	<script type="text/javascript" src="js/lib/jqueryui/jquery-ui.min.js"></script>
	<script type="text/javascript" src="js/lib/lobipanel/lobipanel.min.js"></script>
	<script type="text/javascript" src="js/lib/match-height/jquery.matchHeight.min.js"></script>
	

<script src="js/app.js"></script>
</body>
</html>