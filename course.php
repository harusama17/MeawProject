<?php
    require_once('script/dbcon.php');

    ?>
<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>คอร์ส</title>

	<link href="img/favicon.144x144.png" rel="apple-touch-icon" type="image/png" sizes="144x144">
	<link href="img/favicon.114x114.png" rel="apple-touch-icon" type="image/png" sizes="114x114">
	<link href="img/favicon.72x72.png" rel="apple-touch-icon" type="image/png" sizes="72x72">
	<link href="img/favicon.57x57.png" rel="apple-touch-icon" type="image/png">
	<link href="img/favicon.png" rel="icon" type="image/png">
	<link href="img/favicon.ico" rel="shortcut icon">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="css/lib/lobipanel/lobipanel.min.css">
	<link rel="stylesheet" href="css/separate/vendor/lobipanel.min.css">
	<link rel="stylesheet" href="css/lib/jqueryui/jquery-ui.min.css">
	<link rel="stylesheet" href="css/separate/pages/widgets.min.css">
    <link rel="stylesheet" href="css/lib/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="css/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/separate/pages/prices.min.css">
    <link rel="stylesheet" href="css/lib/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="css/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">
</head>
<body class="with-side-menu control-panel control-panel-compact">

	<?php include 'header.php';?>


	<div class="page-content">
	    <div class="container-fluid">
	        <section class="box-typical box-typical-full-height">
				<div class="box-typical-center">
					<div class="box-typical-center-in prices-page">
						<header class="prices-page-title">คอร์สเรียนทั้งหมด</header>
						<p class="prices-page-subtitle">ด่วน!!! จำนวนจำกัด รีบจองได้เลย</p>

						<?php 

						$sql = "SELECT * FROM `cousrse`";
						$result = $con->query($sql);

						if ($result->num_rows > 0) {
  						// output data of each row
  						while($row = $result->fetch_assoc()) { ?>

  							<article class="price-card">
							<header class="price-card-header"> <?php echo  $row["name"] ?> </header>
							<div class="price-card-body">
								<img src="<?php echo  $row["img"] ?>" width="200" height="200">
								
								<ul class="price-card-list">
									<li><i class="font-icon font-icon-ok"></i><?php echo  $row["description"] ?></li>
									<li><i class="font-icon font-icon-ok"></i>ราคา <?php echo  $row["price"] ?> บาท</li>
								</ul>
								<div class="clear"></div>
								<a href="bookcourse.php?pid=<?php echo  $row["pid"] ?>" class="btn btn-rounded">จองคอร์ส</a>
							</div>
						</article>

						<?php
  						}
						}
						?>

					</div>
				</div>
			</section><!--.box-typical.prices-page-->
	
	    </div><!--.container-fluid-->
	</div><!--.page-content-->



	<script src="js/lib/jquery/jquery-3.2.1.min.js"></script>
	<script src="js/lib/popper/popper.min.js"></script>
	<script src="js/lib/tether/tether.min.js"></script>
	<script src="js/lib/bootstrap/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>

	<script type="text/javascript" src="js/lib/jqueryui/jquery-ui.min.js"></script>
	<script type="text/javascript" src="js/lib/lobipanel/lobipanel.min.js"></script>
	<script type="text/javascript" src="js/lib/match-height/jquery.matchHeight.min.js"></script>
	

<script src="js/app.js"></script>
</body>
</html>