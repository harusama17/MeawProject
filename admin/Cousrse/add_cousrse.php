<?php 
    session_start();

    if(!isset($_SESSION['userid'])){
        $_SESSION['msg'] = "You must log in first";
        header('location: ../../admin_login.php');
    }

    if(isset($_GET['logout'])){
        session_destroy();
        unset($_SESSION['userid']);
        unset($_SESSION['email']);
        unset($_SESSION['username']);
        header('location: ../../admin_login.php');
    }
?>

<!DOCTYPE html>
<html>

<head lang="en">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Admin Panel</title>


    <link href="../../img/favicon.png" rel="icon" type="image/png">
    <link href="../../img/favicon.ico" rel="shortcut icon">

    <link rel="stylesheet" href="../../css/lib/lobipanel/lobipanel.min.css">
    <link rel="stylesheet" href="../../css/separate/vendor/lobipanel.min.css">
    <link rel="stylesheet" href="../../css/lib/jqueryui/jquery-ui.min.css">
    <link rel="stylesheet" href="../../css/separate/pages/widgets.min.css">
    <link rel="stylesheet" href="../../css/lib/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="../../css/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/main.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">


</head>

<body class="with-side-menu control-panel control-panel-compact">

    <?php include '../header.php';?>
    <?php  require_once('../../script/dbcon.php');?>



    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">เพิ่มคอร์ส</h3>
                        </div>
                        <div class="card-body">

                            <form class="row g-3 mt-3" action="add_cousrse_db.php" method="POST"  enctype="multipart/form-data">

                                <?php include('../../errors.php'); ?>
                                <?php if (isset($_SESSION['error'])) : ?>

                                <div class="alert alert-danger" role="alert">
                                    <?php 
                                        echo $_SESSION['error'];
                                        unset($_SESSION['error']);
                                    ?>
                                </div>
                                <?php endif ?>
                                <div class="form-row">
                   
                                    <div class="form-group col-md-6">
                                        <label class="form-label">ชื่อคอร์ส <span style="color: red;">*</span>:</label>
                                        <input type="text" class="form-control" name="name_cousrse" id="name_cousrse" placeholder="ชื่อคอร์ส" required>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label class="form-label">ราคาคอร์ส <span style="color: red;">*</span>:</label>
                                        <input type="text" class="form-control" name="price" id="price"  placeholder="ราคาคอร์ส" onkeypress="return onlyNumberKey(event)"  required>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label class="form-label">อัพโหลดรูป <span style="color: red;">*</span>:</label>
                                        <input type="file" class="form-control" name="fileToUpload" id="fileToUpload" accept="image/*">
                                    </div>
                                </div>
                                <div class="form-row mb-3">
                                    <div class="form-group col-md-6">
                                        <label class="form-label">โปรโมชั่นของคอร์ส <span style="color: red;">*</span>:</label>
                                        <select class="form-control form-select" name="prmotion_id">
                                            <option value="" selected>Select Promotion </option>
                                            <?php 

                                            $sql = "SELECT * FROM `promotion`";
                                            $result = $con->query($sql);

                                            if ($result->num_rows > 0) {

                                                foreach ($result as $key=>$value) {     
                                            ?>
                                                    <option value="<?php  echo $value['rid'] ?>"> <?php  echo $value['name'] ?> - <?php  echo $value['price'] ?> </option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="form-label">รายละเอียดคอร์ส <span style="color: red;">*</span>:</label>
                                        <textarea class="form-control" name="description" id="description" placeholder="รายละเอียดคอร์ส" required></textarea>
                                    </div>
                                </div>
                                <div class="form-row mb-3">
                                    <div class="form-group col-md-6">
                                        <label class="form-label">วันเริ่มเข้าเรียน <span style="color: red;">*</span>:</label>
                                        <input type="date" class="form-control" name="date_start" placeholder="วันเริ่มเข้าเรียน" required>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="form-label">วันสิ้นสุดการเข้าเรียน <span style="color: red;">*</span>:</label>
                                        <input type="date" class="form-control" name="date_end" placeholder="วันสิ้นสุดการเข้าเรียน" required>

                                    </div>
                                    <!-- <div class="form-group col-md-4">
                                        <label class="form-label">ชั่วโมงการเรียนทั้งหมด <span style="color: red;">*</span>:</label>
                                        <input type="text" class="form-control" name="time_cousrse" id="time_cousrse"  placeholder="ชั่วโมงการเรียนทั้งหมด" onkeypress="return onlyNumberKey(event)"  maxlength="2">
                                    </div> -->
                                </div>            
                                <div class=" mb-3" id="time_couserse">
                                   
                                    <div class="row" id="index-1">
                                        <div class="form-group col-md-3" >
                                            <label class="form-label">วันเข้าเรียน  <span style="color: red;">*</span>:</label>
                                            <select class="form-control form-select" name="date_cousrse[]" id="date_cousrse" placeholder="วันเข้าเรียน" required>
                                                <option value="" selected> เลือกวันเข้าเรียน </option>
                                                <option value="1" > จันทร์ </option>
                                                <option value="2" > อังคาร </option>
                                                <option value="3" > พุธ </option>
                                                <option value="4" > พฤหัสบดี </option>
                                                <option value="5" > ศุกร์ </option>
                                                <option value="6" > เสาร์ </option>
                                                <option value="7" > อาทิตย์ </option>
                                            </select>
                                            <!-- <input type="date" class="form-control" name="date_cousrse[]" id="date_cousrse" placeholder="วันที่" required> -->
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label class="form-label">เวลาเริ่ม <span style="color: red;">*</span>:</label>
                                            <input type="time" class="form-control" name="time_start[]" id="time_start" required>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label class="form-label">เวลาสิ้นสุด <span style="color: red;">*</span>:</label>
                                            <input type="time" class="form-control" name="time_end[]" id="time_end" required>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label class="form-label">เพิ่มวัน/เวลา </label>
                                            <button type="button" class="btn btn-success-outline" onclick="AddDateTime()"> เพิ่ม</button>
                                        </div>
                                    </div>
                                        
                                       
                                    
                                    
                                </div>

                                <div class="mb-3 row">
                                    <div class="d-flex justify-content-center position-relative">
                                        <button type="submit" name="insert" class="btn btn-primary ms-2">Submit</button>
                                        <a href="index.php" class="btn btn-danger ms-2">Back</a>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
                <!--.col-->
            </div>
            <!--.row-->

        </div>
        <!--.container-fluid-->
    </div>
    <!--.page-content-->


    <script>
        function onlyNumberKey(evt) {

            // Only ASCII character in that range allowed
            var ASCIICode = (evt.which) ? evt.which : evt.keyCode
            if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
                return false;
            return true;
        }
       
        let index = 1;
        let timeCouserse = document.getElementById('time_couserse');
       
        function AddDateTime(){
            index++;
            var fieldHTML = '<div class="row" id="index-'+index+'">'+
                        '<div class="form-group col-md-3" >'+
                        '<label class="form-label">วันที่ลงคอร์ส <span style="color: red;">*</span>:</label>'+
                        // '<input type="date" class="form-control" name="date_cousrse[]" id="date_cousrse" placeholder="วันที่" required></div>'+
                        '<select class="form-control form-select" name="date_cousrse[]" id="date_cousrse" placeholder="วันเข้าเรียน" required>'+
                                                '<option value="" selected> เลือกวันเข้าเรียน </option>'+
                                                '<option value="1" > จันทร์ </option>'+
                                                '<option value="2" > อังคาร </option>'+
                                                '<option value="3" > พุธ </option>'+
                                                '<option value="4" > พฤหัสบดี </option>'+
                                                '<option value="5" > ศุกร์ </option>'+
                                                '<option value="6" > เสาร์ </option>'+
                                                '<option value="7" > อาทิตย์ </option>'+
                        '</select></div>'+
                        '<div class="form-group col-md-3">'+
                        '<label class="form-label">เวลาเริ่ม <span style="color: red;">*</span>:</label>'+
                        '<input type="time" class="form-control" name="time_start[]" id="time_start" required></div>'+
                        '<div class="form-group col-md-3">'+
                        '<label class="form-label">เวลาสิ้นสุด <span style="color: red;">*</span>:</label>'+
                        '<input type="time" class="form-control" name="time_end[]" id="time_end" required></div>'+
                        '<div class="form-group col-md-3">'+
                        '<label class="form-label">ลบวัน/เวลา </label>'+
                        '<button type="button" class="btn btn-danger-outline" onclick="Removecousrse('+index+')"> &times;</button>'+
                        '</div>'; 
            $(timeCouserse).append(fieldHTML); //Add field html
        }

        function Removecousrse(id){
            document.getElementById('index-'+id).remove();   
            id--;
           
        }
    </script>
   

    <!-- 
    <script src="../../js/lib/jquery/jquery-3.2.1.min.js"></script>
    <script src="../../js/lib/popper/popper.min.js"></script>
    <script src="../../js/lib/tether/tether.min.js"></script>
    <script src="../../js/lib/bootstrap/bootstrap.min.js"></script>
    <script src="../../js/plugins.js"></script> -->
    <script src="../../js/lib/jquery/jquery-3.2.1.min.js"></script>

    <script type="text/javascript" src="../../js/lib/jqueryui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../../js/lib/lobipanel/lobipanel.min.js"></script>
    <script type="text/javascript" src="../../js/lib/match-height/jquery.matchHeight.min.js"></script>
    <script src="../../js/app.js"></script>
</body>

</html>