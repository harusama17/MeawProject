<?php 
    session_start();

    if(!isset($_SESSION['userid'])){
        $_SESSION['msg'] = "You must log in first";
        header('location: ../../admin_login.php');
    }

    if(isset($_GET['logout'])){
        session_destroy();
        unset($_SESSION['userid']);
        unset($_SESSION['email']);
        unset($_SESSION['username']);
        header('location: ../../admin_login.php');
    }
?>

<!DOCTYPE html>
<html>

<head lang="en">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Admin Panel</title>


    <link href="../../img/favicon.png" rel="icon" type="image/png">
    <link href="../../img/favicon.ico" rel="shortcut icon">

    <link rel="stylesheet" href="../../css/lib/lobipanel/lobipanel.min.css">
    <link rel="stylesheet" href="../../css/separate/vendor/lobipanel.min.css">
    <link rel="stylesheet" href="../../css/lib/jqueryui/jquery-ui.min.css">
    <link rel="stylesheet" href="../../css/separate/pages/widgets.min.css">
    <link rel="stylesheet" href="../../css/lib/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="../../css/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/main.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">

</head>

<body class="with-side-menu control-panel control-panel-compact">

    <?php include '../header.php';?>
    <?php  require_once('../../script/dbcon.php');
        $findCousrse = "SELECT * FROM cousrse WHERE pid ='" . $_GET['id'] . "'";
        $result = $con->query($findCousrse);
        $Cousrse = mysqli_fetch_array($result);
    ?>


    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
             
                    <div class="card">
                        <div class="card-header">
                            <div class="d-flex">
                                <div class="p-2 w-100">Summary Report : <?php echo $Cousrse['name'];?></div>
                                <div class="p-2 flex-shrink-1">
                                    <!-- <a href="index.php" class="btn btn-secondary">
                                        Back
                                    </a> -->
                                    <a href="export.php?id=<?php echo $Cousrse["pid"]; ?>" class="btn btn-primary"> Export->Excel </a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                        <?php if (isset($_SESSION['success'])) : ?>
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <strong>
                                    <?php 
                                            echo $_SESSION['success'];
                                            unset($_SESSION['success']);
                                        ?>
                                </strong> 
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            </div>                       
                        <?php endif ?>
    
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Cost</th>
                                        <th scope="col">Discount</th>
                                        <th scope="col">Net Price</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php 
                                    $sql = "SELECT * FROM summary_book WHERE pid ='" . $_GET['id'] . "'";
                                    $result = $con->query($sql);
                                    $Total_Cost = 0;
                                    $Total_Discount = 0;
                                    $Total_Net = 0;

                                    if ($result->num_rows > 0) {
                                  
                                        foreach ($result as $key=>$value) {     
                                            $Total_Cost += $value['cost'];
                                            $Total_Discount += $value['discount'];
                                            $Total_Net += $value['summary_price'];

                                    ?>
                                    <tr>
                                        <th scope="row"> <?php echo  $key+1 ?></th>
                                        <td> <?php echo  $value['cost'] ?> </td>
                                        <td> <?php echo  $value['discount'] ?> </td>
                                        <td> <?php echo  $value['summary_price'] ?> </td>
  
                                    </tr>
                                    <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                                <tfoot >
									<tr class="table-primary">
										<th style="text-align: left;">Total</th>
										<th style="text-align: left;"> <b> <?php echo $Total_Cost ?> </b></th>
                                        <th style="text-align: left;"> <b> <?php echo $Total_Discount ?> </b></th>
										<th style="text-align: left;"> <b> <?php echo $Total_Net ?> </b></th>
									</tr>
								</tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <!--.col-->
            </div>
            <!--.row-->

        </div>
        <!--.container-fluid-->
    </div>
    <!--.page-content-->



    <script src="../../js/lib/jquery/jquery-3.2.1.min.js"></script>
    <script src="../../js/lib/popper/popper.min.js"></script>
    <script src="../../js/lib/tether/tether.min.js"></script>
    <script src="../../js/lib/bootstrap/bootstrap.min.js"></script>
    <script src="../../js/plugins.js"></script>

    <script type="text/javascript" src="../../js/lib/jqueryui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../../js/lib/lobipanel/lobipanel.min.js"></script>
    <script type="text/javascript" src="../../js/lib/match-height/jquery.matchHeight.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>


    <script src="../../js/app.js"></script>
</body>

</html>