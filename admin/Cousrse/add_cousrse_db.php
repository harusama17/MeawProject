<?php 
    session_start();
    require_once('../../script/dbcon.php');

    $errors = array();

    if (isset($_POST['insert'])) {
        $prmotion_id = trim($_POST['prmotion_id']);
        $name = trim($_POST['name_cousrse']);
        $price =  trim($_POST['price']);
        $description = trim($_POST['description']);
        $dateStart= trim($_POST['date_start']);
        $dateEnd = trim($_POST['date_end']);

        if (empty($name)) {
            array_push($errors, "Name is required");
            $_SESSION['error'] = "Name is required";
        }
        if (empty($price)) {
            array_push($errors, "Price is required");
            $_SESSION['error'] = "Price is required";
        }
        if (empty($description)) {
            array_push($errors, "Description is required");
            $_SESSION['error'] = "Description is required";
        }
        if (empty($dateStart)) {
            array_push($errors, "Date Start is required");
            $_SESSION['error'] = "Date Start is required";
        }
        if (empty($dateEnd)) {
            array_push($errors, "Date End is required");
            $_SESSION['error'] = "Date End is required";
        }
      

        if (count($errors) == 0) {
            $path ="";
            if ($_FILES['fileToUpload']["name"] != "")
            {
                $target_dir = "uploads/"; //เก็บpathไฟล์รูป
                $target_file = "../".$target_dir . basename($_FILES["fileToUpload"]["name"]);
                $uploadOk = 1;
                
                // เช็คว่ามีไฟล์อยู่หรือป่าว
                if (file_exists($target_file)) {
                    echo "Sorry, file already exists.";
                    $uploadOk = 0;
                }
                // เช็คขนาดไฟล์
                if ($_FILES["fileToUpload"]["size"] > 500000) {
                    echo "Sorry, your file is too large.";
                    $uploadOk = 0;
                }

                // Check if $uploadOk is set to 0 by an error
                if ($uploadOk == 0) {
                        echo "Sorry, your file was not uploaded.";
                // if everything is ok, try to upload file
                } else {
                    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], "../".$target_file)) {
                        echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
                    } else {
                        echo "Sorry, there was an error uploading your file.";
                    }
                }
                $path = substr($target_file,3);
            }

            if($prmotion_id == ""){
                $sql = "INSERT INTO cousrse (name, img, price, description, start_date, end_date) VALUES ('$name', '$path', '$price', '$description', '$dateStart', '$dateEnd')";
            }
            else{
                $sql = "INSERT INTO cousrse (name, img, price, description, rid, start_date, end_date) VALUES ('$name', '$path', '$price', '$description', '$prmotion_id', '$dateStart', '$dateEnd')";
            }

            mysqli_query($con, $sql);
            $last_id = mysqli_insert_id($con);
            
            $date_cousrse = $_POST['date_cousrse'];
            $time_start = $_POST['time_start'];
            $time_end = $_POST['time_end'];
            // loop insert time
            for($i = 0; $i < count( $date_cousrse); $i++) {
                $first  = new DateTime($time_start[$i]);
                $second = new DateTime($time_end[$i]);
                $diff = $first->diff( $second );
                $hours = $diff->format('%H');
                $minutes = $diff->format('%I');


                $sql = "INSERT INTO duration_cousrse (pid, date_cousrse, time_start, time_end, hours, minutes) VALUES ('$last_id', '$date_cousrse[$i]', '$time_start[$i]', '$time_end[$i]', '$hours', '$minutes')";
                mysqli_query($con, $sql);
            }

            $_SESSION['success'] = "Create item successfully";
    
            header('location: index.php');
        } else {
            header("location: add_cousrse.php");
        }
    }

?>