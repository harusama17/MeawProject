<?php 
    session_start();
    require_once('../../script/dbcon.php');

    $errors = array();

    if (isset($_POST['update'])) {
        $id  = $_POST['id'];
        $prmotion_id = trim($_POST['prmotion_id']);
        $name = trim($_POST['name_cousrse']);
        $price =  trim($_POST['price']);
        $description = trim($_POST['description']);
        $dateStart= trim($_POST['date_start']);
        $dateEnd = trim($_POST['date_end']);

        if (empty($name)) {
            array_push($errors, "Name is required");
            $_SESSION['error'] = "Name is required";
        }
        if (empty($price)) {
            array_push($errors, "Price is required");
            $_SESSION['error'] = "Price is required";
        }
        if (empty($description)) {
            array_push($errors, "Description is required");
            $_SESSION['error'] = "Description is required";
        }
        if (empty($dateStart)) {
            array_push($errors, "Date Start is required");
            $_SESSION['error'] = "Date Start is required";
        }
        if (empty($dateEnd)) {
            array_push($errors, "Date End is required");
            $_SESSION['error'] = "Date End is required";
        }
    
        // if (empty($prmotion_id)) {
        //     array_push($errors, "Prmotion is required");
        //     $_SESSION['error'] = "Prmotion is required";
        // }

        if (count($errors) == 0) {
            $sql = "SELECT * FROM cousrse WHERE pid ='" .  $id . "'";
            $result = $con->query($sql);
            $data = mysqli_fetch_array($result);

            $path =  $data["img"];
            if ($_FILES['fileToUpload']["name"] != "")
            {
                $target_dir = "uploads/"; //เก็บpathไฟล์รูป
                $target_file = "../".$target_dir . basename($_FILES["fileToUpload"]["name"]);
                $uploadOk = 1;
                
                // เช็คว่ามีไฟล์อยู่หรือป่าว
                if (file_exists($target_file)) {
                    echo "Sorry, file already exists.";
                    $uploadOk = 0;
                }
                // เช็คขนาดไฟล์
                if ($_FILES["fileToUpload"]["size"] > 500000) {
                    echo "Sorry, your file is too large.";
                    $uploadOk = 0;
                }

                // Check if $uploadOk is set to 0 by an error
                if ($uploadOk == 0) {
                        echo "Sorry, your file was not uploaded.";
                // if everything is ok, try to upload file
                } else {
                   if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], "../".$target_file)) {
                        echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
                    } else {
                        echo "Sorry, there was an error uploading your file.";
                    }
                }
                $path = substr($target_file,3);
            }

            if(empty($prmotion_id)){
                $sql = "UPDATE `cousrse` SET  `rid`='NULL', `name`='$name', `img`='$path', `price`='$price', `description`='$description', `start_date`='$dateStart', `end_date`='$dateEnd' WHERE  `pid`= '$id';";
            }
            else{
                $sql = "UPDATE `cousrse` SET  `rid`='$prmotion_id',`name`='$name', `img`='$path', `price`='$price', `description`='$description', `start_date`='$dateStart', `end_date`='$dateEnd'  WHERE  `pid`= '$id';";
            }
            mysqli_query($con, $sql);

            // ------------------------------------ loop Update time ------------------------------------ 
            $duration_id = $_POST['duration_id'];
            $date_cousrse = $_POST['date_cousrse'];
            $time_start = $_POST['time_start'];
            $time_end = $_POST['time_end'];
            // print_r( $duration_id);
            // print_r($date_cousrse);

                foreach ($duration_id as $key => $durationId ) {

                    $first  = new DateTime($time_start[$key]);
                    $second = new DateTime($time_end[$key]);
                    $diff = $first->diff( $second );
                    $hours = $diff->format('%H');
                    $minutes = $diff->format('%I');

                    $sql2 = "UPDATE `duration_cousrse` SET  `pid`='$id', `date_cousrse`='$date_cousrse[$key]', `time_start`='$time_start[$key]', `time_end`='$time_end[$key]', `hours`='$hours', `minutes`='$minutes' WHERE  `id`= '$duration_id[$key]';";
                  
                    mysqli_query($con, $sql2);
                }
            
            // ------------------------------------ loop Update time ------------------------------------ 
            // ------------------------------------ loop Insert New time ------------------------------------ 
            
            if(isset($_POST['new_duration_id'])) {
                $new_duration = $_POST['new_duration_id'];
                $new_date_cousrse = $_POST['new_date_cousrse'];
                $new_time_start = $_POST['new_time_start'];
                $new_time_end = $_POST['new_time_end'];
                for($i = 0; $i < count( $new_duration); $i++) {
                    $first  = new DateTime($new_time_start[$i]);
                    $second = new DateTime($new_time_end[$i]);
                    $diff = $first->diff( $second );
                    $hours = $diff->format('%H');
                    $minutes = $diff->format('%I');

                    $sql = "INSERT INTO duration_cousrse (pid, date_cousrse, time_start, time_end, hours, minutes) VALUES ('$id', '$new_date_cousrse[$i]', '$new_time_start[$i]', '$new_time_end[$i]', '$hours', '$minutes')";
                    mysqli_query($con, $sql);
                }
            }
            // ------------------------------------ loop Insert New time ------------------------------------ 
            // ------------------------------------ loop Delete time ------------------------------------ 
           
             if(isset($_POST['remove_id'])) {
                $ArrayRm = explode(",",$_POST['remove_id'][0]);
                foreach ($ArrayRm as $key2 => $rmID ) {
                    $sql3 = "DELETE FROM duration_cousrse WHERE id = ".$rmID;
                    mysqli_query($con , $sql3);
                }
            }
            // ------------------------------------ loop Delete time ------------------------------------ 
                

            $_SESSION['success'] = "Update item successfully";
            header('location: index.php');
        } else {
            header("location: edit.php");
        }
    }

?>