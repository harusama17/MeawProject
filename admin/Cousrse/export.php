
<?php 
    session_start();
    require_once('../../script/dbcon.php');


    // Filter the excel data 
    function filterData(&$str){ 
        $str = preg_replace("/\t/", "\\t", $str); 
        $str = preg_replace("/\r?\n/", "\\n", $str); 
        if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"'; 
    } 
    // Excel file name for download 
    $fileName = "reprot_" . date('Y-m-d') . ".xls"; 

    // Column names 
    $fields = array('Number', 'Cost', 'Discount', 'Net Price'); 
 
    // Display column names as first row 
    $excelData = implode("\t", array_values($fields)) . "\n"; 

    // Fetch records from database 
    $sql = "SELECT * FROM summary_book WHERE pid ='" . $_GET['id'] . "'";
    $result = $con->query($sql);

    if($result->num_rows > 0){ 
        // Output each row of the data 
        $idx = 1;
        $Total_Cost = 0;
        $Total_Discount = 0;
        $Total_Net = 0;
        while($row = $result->fetch_assoc()){ 

            $Total_Cost += $row['cost'];
            $Total_Discount += $row['discount'];
            $Total_Net += $row['summary_price'];

            $lineData = array($idx, $row['cost'], $row['discount'], $row['summary_price']); 
            array_walk($lineData, 'filterData'); 
            $excelData .= implode("\t", array_values($lineData)) . "\n"; 
            $idx++;
        } 
    }else{ 
        $excelData .= 'No records found...'. "\n"; 
    } 
    $fields2 = array('Total',  $Total_Cost,  $Total_Discount , $Total_Net); 
    $excelData .= implode("\t", array_values($fields2)) . "\n"; 

    // var_dump($excelData);
    // Headers for download 
    header("Content-Type: application/vnd.ms-excel"); 
    header("Content-Disposition: attachment; filename=\"$fileName\""); 
    
    // Render excel data 
    echo $excelData; 

exit;
