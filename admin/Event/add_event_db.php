<?php 
    session_start();
    require_once('../../script/dbcon.php');

    $errors = array();
    
    if (isset($_POST['insert'])) {
        $name = trim($_POST['name_event']);
        $place =  trim($_POST['place']);
        $date_event = $_POST['date_event'];
        $description = trim($_POST['description']);
        

        if (empty($name)) {
            array_push($errors, "Name is required");
            $_SESSION['error'] = "Name is required";
        }
        if (empty($place)) {
            array_push($errors, "Place is required");
            $_SESSION['error'] = "Place is required";
        }
        if (empty($date_event)) {
            array_push($errors, "Date Event is required");
            $_SESSION['error'] = "Date Event is required";
        }
        if (empty($description)) {
            array_push($errors, "Description is required");
            $_SESSION['error'] = "Description is required";
        }
        // var_dump($course_id ,$name, $description,empty($name),empty($description),$errors );

        if (count($errors) == 0) {
            $path ="";
            if ($_FILES['fileToUpload']["name"] != "")
            {
                $target_dir = "uploads/"; //เก็บpathไฟล์รูป
                $target_file = "../".$target_dir . basename($_FILES["fileToUpload"]["name"]);
                $uploadOk = 1;

                // เช็คว่ามีไฟล์อยู่หรือป่าว
                if (file_exists($target_file)) {
                    echo "Sorry, file already exists.";
                    $uploadOk = 0;
                }
                // เช็คขนาดไฟล์
                if ($_FILES["fileToUpload"]["size"] > 500000) {
                    echo "Sorry, your file is too large.";
                    $uploadOk = 0;
                }

                // Check if $uploadOk is set to 0 by an error
                if ($uploadOk == 0) {
                        echo "Sorry, your file was not uploaded.";
                // if everything is ok, try to upload file
                } else {
                   if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], "../".$target_file)) {
                        echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
                    } else {
                        echo "Sorry, there was an error uploading your file.";
                    }
                }
                $path = substr($target_file,3);
            }
           
            $sql = "INSERT INTO event (name, place, img, description, date) VALUES ('$name', '$place', '$path', '$description', '$date_event')";

            
            mysqli_query($con, $sql);
            $_SESSION['success'] = "Create item successfully";
    
            header('location: index.php');
        } else {
            header("location: add_event.php");
        }
    }

?>