<?php 
    session_start();
    require_once('../../script/dbcon.php');

    $errors = array();

    if (isset($_POST['update'])) {
        $id  = $_POST['id'];
        $name = trim($_POST['name_coach']);
        $phone = trim($_POST['phone']);
        $email =  trim($_POST['email']);
        $line = $_POST['line'];
        $facebook = trim($_POST['facebook']);
        
 
        if (empty($name)) {
            array_push($errors, "Name is required");
            $_SESSION['error'] = "Name is required";
        }
        if (empty($phone)) {
            array_push($errors, "Phone is required");
            $_SESSION['error'] = "Phone is required";
        }
        if (empty($email)) {
            array_push($errors, "E-mail is required");
            $_SESSION['error'] = "E-mail is required";
        }
        if (empty($line)) {
            array_push($errors, "Line is required");
            $_SESSION['error'] = "Line is required";
        }
        if (empty($facebook)) {
            array_push($errors, "Facebook is required");
            $_SESSION['error'] = "Facebook is required";
        }

        if (count($errors) == 0) {
            $sql = "SELECT * FROM coach WHERE oid ='" .  $id . "'";
            $result = $con->query($sql);
            $data = mysqli_fetch_array($result);

            $path =  $data["img"];
            if ($_FILES['fileToUpload']["name"] != "")
            {
                $target_dir = "uploads/"; //เก็บpathไฟล์รูป
                $target_file = "../".$target_dir . basename($_FILES["fileToUpload"]["name"]);
                $uploadOk = 1;
                
                // เช็คว่ามีไฟล์อยู่หรือป่าว
                if (file_exists($target_file)) {
                    echo "Sorry, file already exists.";
                    $uploadOk = 0;
                }
                // เช็คขนาดไฟล์
                if ($_FILES["fileToUpload"]["size"] > 500000) {
                    echo "Sorry, your file is too large.";
                    $uploadOk = 0;
                }

                // Check if $uploadOk is set to 0 by an error
                if ($uploadOk == 0) {
                        echo "Sorry, your file was not uploaded.";
                // if everything is ok, try to upload file
                } else {
                   if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], "../".$target_file)) {
                        echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
                    } else {
                        echo "Sorry, there was an error uploading your file.";
                    }
                }
                $path = substr($target_file,3);
            }

            $sql = "UPDATE `coach` SET  `name`='$name', `img`='$path', `phone`='$phone', `email`='$email', `line`='$line', `facebook`='$facebook' WHERE  `oid`= '$id';";
            mysqli_query($con, $sql);
            $_SESSION['success'] = "Create item successfully";
    
            header('location: index.php');
        } else {
            header("location: edit.php");
        }
    }

?>