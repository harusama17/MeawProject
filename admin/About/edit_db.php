<?php 
    session_start();
    require_once('../../script/dbcon.php');

    $errors = array();

    if (isset($_POST['update'])) {
        $id  = $_POST['id'];
        $description = trim($_POST['description']);
        
      
        if (empty($description)) {
            array_push($errors, "Description is required");
            $_SESSION['error'] = "Description is required";
        }

        if (count($errors) == 0) {
            $sql = "SELECT * FROM `about` WHERE aid ='" .  $id . "'";
            $result = $con->query($sql);
            $data = mysqli_fetch_array($result);
          
            $path =  $data["img"];
            if ($_FILES['fileToUpload']["name"] != "")
            {
                $target_dir = "uploads/"; //เก็บpathไฟล์รูป
                $target_file = "../".$target_dir . basename($_FILES["fileToUpload"]["name"]);
                $uploadOk = 1;
                
                // เช็คว่ามีไฟล์อยู่หรือป่าว
                if (file_exists($target_file)) {
                    echo "Sorry, file already exists.";
                    $uploadOk = 0;
                }
                // เช็คขนาดไฟล์
                if ($_FILES["fileToUpload"]["size"] > 500000) {
                    echo "Sorry, your file is too large.";
                    $uploadOk = 0;
                }

                // Check if $uploadOk is set to 0 by an error
                if ($uploadOk == 0) {
                        echo "Sorry, your file was not uploaded.";
                // if everything is ok, try to upload file
                } else {
                   if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], "../".$target_file)) {
                        echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
                    } else {
                        echo "Sorry, there was an error uploading your file.";
                    }
                }
                $path = substr($target_file,3);
            }
            $sql = "UPDATE database_academy.about SET img = '$path', description ='$description' WHERE aid= '$id';";
           
            mysqli_query($con, $sql);
            $_SESSION['success'] = "Update item successfully";
    
            header('location: index.php');
        } else {
            header( "location: edit.php?id={$id}" );
        }
    }

?>