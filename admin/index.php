<?php 
    session_start();

    if(!isset($_SESSION['userid'])){
        $_SESSION['msg'] = "You must log in first";
        header('location: ../admin_login.php');
    }

    if(isset($_GET['logout'])){
        session_destroy();
        unset($_SESSION['userid']);
        unset($_SESSION['email']);
        unset($_SESSION['username']);
        header('location: ../admin_login.php');
    }
?>


<!DOCTYPE html>
<html>

<head lang="en">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Admin Panel</title>


    <link href="../img/favicon.png" rel="icon" type="image/png">
    <link href="../img/favicon.ico" rel="shortcut icon">

    <link rel="stylesheet" href="../css/lib/lobipanel/lobipanel.min.css">
    <link rel="stylesheet" href="../css/separate/vendor/lobipanel.min.css">
    <link rel="stylesheet" href="../css/lib/jqueryui/jquery-ui.min.css">
    <link rel="stylesheet" href="../css/separate/pages/widgets.min.css">
    <link rel="stylesheet" href="../css/lib/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="../css/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="../css/main.css">

</head>

<body class="with-side-menu control-panel control-panel-compact">

    <?php include 'header.php';?>

    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                    <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                            <div class="card">
                                <a href="../admin/About/" class="btn btn-primary">
                                    <div class="card-body">
                                        <h5 class="card-title">About</h5>
                                        <p class="card-text">จัดการ About</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                            <div class="card">
                                <a href="../admin/Event/" class="btn btn-primary">
                                    <div class="card-body">
                                        <h5 class="card-title">Event</h5>
                                        <p class="card-text">จัดการ Event</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                            <div class="card">
                                <a href="../admin/Promotion/" class="btn btn-primary">
                                    <div class="card-body">
                                        <h5 class="card-title">Promotion</h5>
                                        <p class="card-text">จัดการ Promotion</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                            <div class="card">
                                <a href="../admin/Coach/" class="btn btn-primary">
                                    <div class="card-body">
                                        <h5 class="card-title">Coach</h5>
                                        <p class="card-text">จัดการ Coach</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                            <div class="card">
                            <a href="../admin/Cousrse/" class="btn btn-primary">
                                    <div class="card-body">
                                        <h5 class="card-title">Cousrse</h5>
                                        <p class="card-text">จัดการ Cousrse</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                            <div class="card">
                                <a href="../admin/BookingCousrse/" class="btn btn-primary">
                                    <div class="card-body">
                                        <h5 class="card-title">Cousrse Booking</h5>
                                        <p class="card-text">จัดการ Cousrse Booking</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                            <div class="card">
                                <a href="../admin/Contact" class="btn btn-primary">
                                    <div class="card-body">
                                        <h5 class="card-title">Contact</h5>
                                        <p class="card-text">จัดการ Contact</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>

                </div>
                <!--.col-->
            </div>
            <!--.row-->

        </div>
        <!--.container-fluid-->
    </div>
    <!--.page-content-->



    <script src="../js/lib/jquery/jquery-3.2.1.min.js"></script>
    <script src="../js/lib/popper/popper.min.js"></script>
    <script src="../js/lib/tether/tether.min.js"></script>
    <script src="../js/lib/bootstrap/bootstrap.min.js"></script>
    <script src="../js/plugins.js"></script>

    <script type="text/javascript" src="../js/lib/jqueryui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../js/lib/lobipanel/lobipanel.min.js"></script>
    <script type="text/javascript" src="../js/lib/match-height/jquery.matchHeight.min.js"></script>


    <script src="../js/app.js"></script>
</body>

</html>