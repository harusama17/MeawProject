<header class="site-header">
    <div class="container-fluid">
        <a href="../../admin/index.php" class="site-logo">
            <img class="hidden-md-down" src="../../img/logoo.jpg" alt="">
        </a>


        <div class="site-header-content">
            <div class="site-header-content-in">
                <div class="site-header-shown">


                    <div class="dropdown user-menu">
                        <button class="dropdown-toggle" id="dd-user-menu" type="button" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            <img src="../../img/avatar-2-64.png" alt="">
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dd-user-menu">
                            <a class="dropdown-item" href="#"><span
                                    class="font-icon glyphicon glyphicon-user"></span>โปรไฟล์</a>


                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="index.php?logout='1'"><span
                                    class="font-icon glyphicon glyphicon-log-out"></span>Logout</a>
                        </div>
                    </div>
                </div>
                <div class="site-header-collapsed">
	                    <div class="site-header-collapsed-in">
	                       
	                        <div class="dropdown dropdown-typical">
	                            <a class="dropdown-toggle no-arr"  href="../../admin/index.php" >
	                                <span class="lbl">หน้าแอดมิน</span>
	                            </a>
	                        </div>
                            <div class="dropdown dropdown-typical">
                                <a class="dropdown-toggle no-arr"  href="../../admin/About/" >
	                                <span class="lbl">About</span>
	                            </a>
	                        </div>
                            <div class="dropdown dropdown-typical">
	                            <a class="dropdown-toggle no-arr"  href="../../admin/Event/" >
	                                <span class="lbl">Event</span>
	                            </a>
	                        </div>
                            <div class="dropdown dropdown-typical">
                                <a class="dropdown-toggle no-arr"  href="../../admin/Promotion/" >
	                                <span class="lbl">Promotion</span>
	                            </a>
	                        </div>
                            <div class="dropdown dropdown-typical">
                                 <a class="dropdown-toggle no-arr"  href="../../admin/Coach/" >
	                                <span class="lbl">Coach</span>
	                            </a>
	                        </div>
                            <div class="dropdown dropdown-typical">
                                <a class="dropdown-toggle no-arr"  href="../../admin/Cousrse/" >
	                                <span class="lbl">Cousrse</span>
	                            </a>
	                        </div>
                            <div class="dropdown dropdown-typical">
                                <a class="dropdown-toggle no-arr"  href="../../admin/BookingCousrse/" >
	                                <span class="lbl">Cousrse Booking</span>
	                            </a>
	                        </div>
                            <div class="dropdown dropdown-typical">
                                <a class="dropdown-toggle no-arr"  href="../../admin/Contact/" >
	                                <span class="lbl">Contact</span>
	                            </a>
	                        </div>
	                       
	                        
	                    </div><!--.site-header-collapsed-in-->
	                </div><!--.site-header-collapsed-->
                <!--.site-header-shown-->
            </div>
            <!--site-header-content-in-->
        </div>
        <!--.site-header-content-->
    </div>
    <!--.container-fluid-->
</header>
<!--.site-header-->