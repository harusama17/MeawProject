<?php 
    session_start();

    if(!isset($_SESSION['userid'])){
        $_SESSION['msg'] = "You must log in first";
        header('location: ../../admin_login.php');
    }

    if(isset($_GET['logout'])){
        session_destroy();
        unset($_SESSION['userid']);
        unset($_SESSION['email']);
        unset($_SESSION['username']);
        header('location: ../../admin_login.php');
    }
?>

<!DOCTYPE html>
<html>

<head lang="en">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Admin Panel</title>


    <link href="../../img/favicon.png" rel="icon" type="image/png">
    <link href="../../img/favicon.ico" rel="shortcut icon">

    <link rel="stylesheet" href="../../css/lib/lobipanel/lobipanel.min.css">
    <link rel="stylesheet" href="../../css/separate/vendor/lobipanel.min.css">
    <link rel="stylesheet" href="../../css/lib/jqueryui/jquery-ui.min.css">
    <link rel="stylesheet" href="../../css/separate/pages/widgets.min.css">
    <link rel="stylesheet" href="../../css/lib/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="../../css/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/main.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">


</head>

<body class="with-side-menu control-panel control-panel-compact">

    <?php include '../header.php';?>
    <?php  require_once('../../script/dbcon.php');?>
    <?php 
        $sql = "SELECT * FROM user_book WHERE uid ='" . $_GET['id'] . "'";
        $result = $con->query($sql);
        $data = mysqli_fetch_array($result);
    ?>


    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">แก้ไขข้อมูลผู้จองคอร์ส</h3>
                        </div>
                        <div class="card-body">


                            <form class="row g-3 mt-3" action="edit_db.php" method="POST" enctype="multipart/form-data">

                                <input type="hidden" name="id" value="<?php echo $data['uid'];?>" >

                                <?php include('../../errors.php'); ?>
                                <?php if (isset($_SESSION['error'])) : ?>

                                <div class="alert alert-danger" role="alert">
                                    <?php 
                                        echo $_SESSION['error'];
                                        unset($_SESSION['error']);
                                    ?>
                                </div>
                                <?php endif ?>

                                <div class="form-row">
                   
                                    <div class="form-group col-md-4">
                                        <label class="form-label">ชื่อ - สกุลผู้จองคอร์ส <span style="color: red;">*</span>:</label>
                                        <input type="text" class="form-control" name="name_booking" id="name_booking" placeholder="ชื่อ - สกุลผู้จองคอร์ส"  value="<?php echo $data['name'];?>" required>
                                    </div>
                                    <div class="form-group col-md-8">
                                        <label class="form-label">ข้อมูลติดต่อ <span style="color: red;">*</span>:</label>
                                        <input type="text" class="form-control" name="address" id="address"  placeholder="ข้อมูลติดต่อ" value="<?php echo $data['address'];?>" required>
                                    </div>
                                 
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-2">
                                        <label class="form-label">เบอร์โทรศัพท์ <span style="color: red;">*</span>:</label>
                                        <input type="number" class="form-control" name="phone" id="phone" value="<?php echo $data['phone'] ?>" placeholder="เบอร์โทรศัพท์" onkeypress="return onlyNumberKey(event)"  required>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label class="form-label">E-mail <span style="color: red;">*</span>:</label>
                                        <input type="email" class="form-control" name="email" id="email" placeholder="E-mail"  value="<?php echo $data['email'];?>" required>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label class="form-label">ไลน์ไอดี :</label>
                                        <input type="text" class="form-control" name="line_id" id="line_id"  placeholder="ไลน์ไอดี" value="<?php echo $data['line_id'];?>" >
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label class="form-label">อัพโหลดรูป :</label>
                                        <input type="file" class="form-control" name="fileToUpload" id="fileToUpload" accept="image/*">
                                    </div>
                                </div>
                                 

                                <div class="mb-3 row">
                                    <div class="d-flex justify-content-center position-relative">
                                        <button type="submit" name="update" class="btn btn-primary ms-2">Submit</button>
                                        <a href="index.php" class="btn btn-danger ms-2">Back</a>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
                <!--.col-->
            </div>
            <!--.row-->

        </div>
        <!--.container-fluid-->
    </div>
    <!--.page-content-->

    <script>
        function onlyNumberKey(evt) {
            
            // Only ASCII character in that range allowed
            var ASCIICode = (evt.which) ? evt.which : evt.keyCode
            if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
                return false;
            return true;
        }
    </script>                                             


    <!-- 
    <script src="../../js/lib/jquery/jquery-3.2.1.min.js"></script>
    <script src="../../js/lib/popper/popper.min.js"></script>
    <script src="../../js/lib/tether/tether.min.js"></script>
    <script src="../../js/lib/bootstrap/bootstrap.min.js"></script>
    <script src="../../js/plugins.js"></script> -->

    <script src="../../js/lib/jquery/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="../../js/lib/jqueryui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../../js/lib/lobipanel/lobipanel.min.js"></script>
    <script type="text/javascript" src="../../js/lib/match-height/jquery.matchHeight.min.js"></script>
    <script src="../../js/app.js"></script>
</body>

</html>