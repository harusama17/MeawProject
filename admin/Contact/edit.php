<?php 
    session_start();

    if(!isset($_SESSION['userid'])){
        $_SESSION['msg'] = "You must log in first";
        header('location: ../../admin_login.php');
    }

    if(isset($_GET['logout'])){
        session_destroy();
        unset($_SESSION['userid']);
        unset($_SESSION['email']);
        unset($_SESSION['username']);
        header('location: ../../admin_login.php');
    }
?>

<!DOCTYPE html>
<html>

<head lang="en">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Admin Panel</title>


    <link href="../../img/favicon.png" rel="icon" type="image/png">
    <link href="../../img/favicon.ico" rel="shortcut icon">

    <link rel="stylesheet" href="../../css/lib/lobipanel/lobipanel.min.css">
    <link rel="stylesheet" href="../../css/separate/vendor/lobipanel.min.css">
    <link rel="stylesheet" href="../../css/lib/jqueryui/jquery-ui.min.css">
    <link rel="stylesheet" href="../../css/separate/pages/widgets.min.css">
    <link rel="stylesheet" href="../../css/lib/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="../../css/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/main.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">


</head>

<body class="with-side-menu control-panel control-panel-compact">

    <?php include '../header.php';?>
    <?php  require_once('../../script/dbcon.php');?>
    <?php 
        $sql = "SELECT * FROM contact WHERE cid ='" . $_GET['id'] . "'";
        $result = $con->query($sql);
        $data = mysqli_fetch_array($result);
    ?>


    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">แก้ไขโค๊ช</h3>
                        </div>
                        <div class="card-body">


                            <form class="row g-3 mt-3" action="edit_db.php" method="POST" enctype="multipart/form-data">

                                <input type="hidden" name="id" value="<?php echo $data['cid'];?>" >

                                <?php include('../../errors.php'); ?>
                                <?php if (isset($_SESSION['error'])) : ?>

                                <div class="alert alert-danger" role="alert">
                                    <?php 
                                        echo $_SESSION['error'];
                                        unset($_SESSION['error']);
                                    ?>
                                </div>
                                <?php endif ?>

                                <div class="mb-3 row">
                                    <label class="col-sm-2 col-form-label">ชื่อโค๊ช <span style="color: red;">*</span> :</label>   
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="name_contact" id="name_contact" placeholder="ชื่อโค๊ช" value="<?php echo $data['name'] ?>" required>
                                    </div>
                                </div>
                                <div class="mb-3 row">
                                    <label class="col-sm-2 col-form-label">เบอร์โทรศัพท์ <span style="color: red;">*</span> :</label>   
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="phone" id="phone" placeholder="เบอร์โทรศัพท์"  value="<?php echo $data['phone'] ?>" maxlength="10" minlength="10"  onkeypress="return onlyNumberKey(event)" required>
                                    </div>
                                </div>
                                <div class="mb-3 row">
                                    <label class="col-sm-2 col-form-label">อีเมล์ <span style="color: red;">*</span> :</label>   
                                    <div class="col-sm-10">
                                        <input type="email" class="form-control" name="email" id="email" placeholder="อีเมล์" value="<?php echo $data['email'] ?>" required>
                                    </div>
                                </div>
                                <div class="mb-3 row">
                                    <label class="col-sm-2 col-form-label">รายละเอียดโปรโมชั่น <span
                                            style="color: red;">*</span> :</label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control" name="description" id="description"
                                            placeholder="รายละเอียดโปรโมชั่น" required> <?php echo $data['description'] ?>
                                        </textarea>
                                    </div>
                                </div>

                                <div class="mb-3 row">
                                    <div class="d-flex justify-content-center position-relative">
                                        <button type="submit" name="update" class="btn btn-primary ms-2">Submit</button>
                                        <a href="index.php" class="btn btn-danger ms-2">Back</a>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
                <!--.col-->
            </div>
            <!--.row-->

        </div>
        <!--.container-fluid-->
    </div>
    <!--.page-content-->

    <script>
        function onlyNumberKey(evt) {
            
            // Only ASCII character in that range allowed
            var ASCIICode = (evt.which) ? evt.which : evt.keyCode
            if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
                return false;
            return true;
        }
    </script>                                             


    <!-- 
    <script src="../../js/lib/jquery/jquery-3.2.1.min.js"></script>
    <script src="../../js/lib/popper/popper.min.js"></script>
    <script src="../../js/lib/tether/tether.min.js"></script>
    <script src="../../js/lib/bootstrap/bootstrap.min.js"></script>
    <script src="../../js/plugins.js"></script> -->

    <script type="text/javascript" src="../../js/lib/jqueryui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../../js/lib/lobipanel/lobipanel.min.js"></script>
    <script type="text/javascript" src="../../js/lib/match-height/jquery.matchHeight.min.js"></script>
    <script src="../../js/app.js"></script>
</body>

</html>