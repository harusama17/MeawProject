<?php 
    session_start();
    require_once('../../script/dbcon.php');

    $errors = array();

    if (isset($_POST['insert'])) {
        $name = trim($_POST['name_contact']);
        $phone = trim($_POST['phone']);
        $email =  trim($_POST['email']);
        $description = trim($_POST['description']);
        
        if (empty($name)) {
            array_push($errors, "Name is required");
            $_SESSION['error'] = "Name is required";
        }
        if (empty($phone)) {
            array_push($errors, "Phone is required");
            $_SESSION['error'] = "Phone is required";
        }
        if (empty($email)) {
            array_push($errors, "E-mail is required");
            $_SESSION['error'] = "E-mail is required";
        }
        if (empty($description)) {
            array_push($errors, "Description is required");
            $_SESSION['error'] = "Description is required";
        }

        if (count($errors) == 0) {
           
            $sql = "INSERT INTO contact (name, phone, email, description) VALUES ('$name', '$phone', '$email', '$description')";
            
            mysqli_query($con, $sql);
            $_SESSION['success'] = "Create item successfully";
    
            header('location: index.php');
        } else {
            header("location: add_about.php");
        }
    }

?>