
<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>โปรโมชั่น</title>

	<link href="img/favicon.144x144.png" rel="apple-touch-icon" type="image/png" sizes="144x144">
	<link href="img/favicon.114x114.png" rel="apple-touch-icon" type="image/png" sizes="114x114">
	<link href="img/favicon.72x72.png" rel="apple-touch-icon" type="image/png" sizes="72x72">
	<link href="img/favicon.57x57.png" rel="apple-touch-icon" type="image/png">
	<link href="img/favicon.png" rel="icon" type="image/png">
	<link href="img/favicon.ico" rel="shortcut icon">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="css/lib/lobipanel/lobipanel.min.css">
	<link rel="stylesheet" href="css/separate/vendor/lobipanel.min.css">
	<link rel="stylesheet" href="css/lib/jqueryui/jquery-ui.min.css">
	<link rel="stylesheet" href="css/separate/pages/widgets.min.css">
    <link rel="stylesheet" href="css/lib/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="css/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/separate/pages/prices.min.css">
    <link rel="stylesheet" href="css/lib/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="css/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">
</head>
<body class="with-side-menu control-panel control-panel-compact">


	<?php include 'header.php';?>

	
		<div class="page-content">
	    <div class="container-fluid">
	        <section class="box-typical box-typical-full-height">
				<div class="box-typical-center">
					<div class="box-typical-center-in prices-page">
                        <h1 class="card-title">โปรโมชั่นลดราคา!!!</h1> <br>
                                            
                       


							<?php  require_once('script/dbcon.php');?>
							<?php 
							$Today = date("Y-m-d");
							$HourCousrse = 0;
							$MinuteCousrse = 0;

                            $sql = "SELECT * FROM `cousrse`";
                            $result = $con->query($sql);

                            if ($result->num_rows > 0) {
                                // output data of each row
                                while($row = $result->fetch_assoc()) { 
									if (isset($row['rid'])) { 
										
										$sqlPromo = "SELECT * FROM promotion  WHERE rid ='" . $row['rid'] . "'";
										$resultPromo = $con->query($sqlPromo);
										$Promotions = mysqli_fetch_array($resultPromo);
										
										$sql2 = "SELECT * FROM duration_cousrse WHERE pid ='" . $row['pid'] . "'";
										$result2 = $con->query($sql2);
										if ($result2->num_rows > 0) {
											foreach ($result2 as $key2 => $value2) {     
												$HourCousrse += $value2['hours'];
												$MinuteCousrse += $value2['minutes'];
											}
										}

										if( $Today < $Promotions["end_time"] ){
										
									
                            ?>
                                <div class="tm-row tm-content-row">
                                    <img id="output_img" src="<?php echo $row["img"] ?>" width="250px" /> &nbsp;&nbsp;&nbsp;
                                    <div class="col-sm-12"><br>
                                            
                                        <h2 class="card-title"> ชื่อคอร์ส : <?php echo $row["name"]; ?> </h2>
                                        <h5 class="card-text"> รายละเอียดคอร์ส : <?php echo $row["description"]; ?>  </h5>
                                        <h5 class="card-text"> ราคาคอร์สลดเหลือ :  <?php echo $row["price"]-$Promotions["price"]; ?>    </h5>
										<h5 class="card-text"> ระยะเวลาเข้าเรียน :  
											<?php $date = new DateTime($row['start_date']); echo $date->format('d-m-Y') ?>    
											- 
											<?php $date = new DateTime($row['end_date']); echo $date->format('d-m-Y') ?>    
										</h5>
										<h5 class="card-text"> ชั่วโมงการเข้าเรียนคอร์ส : <?php echo $HourCousrse; ?> ชั่วโมง <?php echo $MinuteCousrse; ?> นาที  </h5>


                                        <h5 class="card-text"> วันเข้าเรียน :    
											<div class="span">
												<?php 
										
													if ($result2->num_rows > 0) {
														foreach ($result2 as $key2 => $value2) {     
									
															switch ($value2['date_cousrse']) {
																case 1:
																echo "วันจันทร์";
																break;
																case 2:
																echo "วันอังคาร";
																break;
																case 3:
																echo "วันพุธ";
																break;
																case 4:
																echo "วันพฤหัสบดี";
																break;
																case 5:
																echo "วันศุกร์";
																break;
																case 6:
																echo "วันเสาร์";
																break;
																case 7:
																echo "วันอาทิตย์";
																break;
															}
												 ?> :
												<span class="card-text"> <?php  echo $value2['time_start']  ?> - <?php  echo $value2['time_end']  ?> </span>
												<br/>
												<?php
														}
													}
												?>
											</div>
										</h5>
										<a href="cart.php?p_id=<?php echo  $row["pid"] ?>&act=add" class="btn btn-rounded" >จองคอร์ส</a>
                                         
                                    </div><!-- tm-col-12 tm-intro-col-l -->

                                  
                                 
                                </div> <!-- tm-row tm-content-row -->
                            
                            <br>
                            <hr/>  
                            <br>
                            <?php
										}
									}
                            	}
                            }
                            ?>


                     </div>
				</div>
			</section><!--.box-typical.prices-page-->
	
	    </div><!--.container-fluid-->
	</div><!--.page-content-->



	<script src="js/lib/jquery/jquery-3.2.1.min.js"></script>
	<script src="js/lib/popper/popper.min.js"></script>
	<script src="js/lib/tether/tether.min.js"></script>
	<script src="js/lib/bootstrap/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>

	<script type="text/javascript" src="js/lib/jqueryui/jquery-ui.min.js"></script>
	<script type="text/javascript" src="js/lib/lobipanel/lobipanel.min.js"></script>
	<script type="text/javascript" src="js/lib/match-height/jquery.matchHeight.min.js"></script>
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<script>
		$(document).ready(function() {
            $('.panel').each(function () {
                try {
                    $(this).lobiPanel({
                        sortable: true
                    }).on('dragged.lobiPanel', function(ev, lobiPanel){
                        $('.dahsboard-column').matchHeight();
                    });
                } catch (err) {}
            });

			google.charts.load('current', {'packages':['corechart']});
			google.charts.setOnLoadCallback(drawChart);
			function drawChart() {
				var dataTable = new google.visualization.DataTable();
				dataTable.addColumn('string', 'Day');
				dataTable.addColumn('number', 'Values');
				// A column for custom tooltip content
				dataTable.addColumn({type: 'string', role: 'tooltip', 'p': {'html': true}});
				dataTable.addRows([
					['MON',  130, ' '],
					['TUE',  130, '130'],
					['WED',  180, '180'],
					['THU',  175, '175'],
					['FRI',  200, '200'],
					['SAT',  170, '170'],
					['SUN',  250, '250'],
					['MON',  220, '220'],
					['TUE',  220, ' ']
				]);

				var options = {
					height: 314,
					legend: 'none',
					areaOpacity: 0.18,
					axisTitlesPosition: 'out',
					hAxis: {
						title: '',
						textStyle: {
							color: '#fff',
							fontName: 'Proxima Nova',
							fontSize: 11,
							bold: true,
							italic: false
						},
						textPosition: 'out'
					},
					vAxis: {
						minValue: 0,
						textPosition: 'out',
						textStyle: {
							color: '#fff',
							fontName: 'Proxima Nova',
							fontSize: 11,
							bold: true,
							italic: false
						},
						baselineColor: '#16b4fc',
						ticks: [0,25,50,75,100,125,150,175,200,225,250,275,300,325,350],
						gridlines: {
							color: '#1ba0fc',
							count: 15
						}
					},
					lineWidth: 2,
					colors: ['#fff'],
					curveType: 'function',
					pointSize: 5,
					pointShapeType: 'circle',
					pointFillColor: '#f00',
					backgroundColor: {
						fill: '#008ffb',
						strokeWidth: 0,
					},
					chartArea:{
						left:0,
						top:0,
						width:'100%',
						height:'100%'
					},
					fontSize: 11,
					fontName: 'Proxima Nova',
					tooltip: {
						trigger: 'selection',
						isHtml: true
					}
				};

				var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
				chart.draw(dataTable, options);
			}
			$(window).resize(function(){
				drawChart();
				setTimeout(function(){
				}, 1000);
			});
		});
	</script>

<script src="js/app.js"></script>
</body>
</html>