<header class="site-header">
	    <div class="container-fluid">
	        <a href="#" class="site-logo">
	            <img class="hidden-md-down" src="img/logoo.jpg" alt="">
	        </a>
	
	        
	       <div class="site-header-content">
	            <div class="site-header-content-in">
	                <div class="site-header-shown">
	                    

	                    <button type="button" class="burger-right">
	                        <i class="font-icon-menu-addl"></i>
	                    </button>
	                </div><!--.site-header-shown-->
	
	                <div class="mobile-menu-right-overlay"></div>
	                <div class="site-header-collapsed">
	                    <div class="site-header-collapsed-in">
	                       
	                        <div class="dropdown dropdown-typical">
	                            <a class="dropdown-toggle no-arr" href="index.php" >
	    
	                                <span class="lbl">หน้าแรก</span>
	                            </a>
	                        </div>

	                        <div class="dropdown dropdown-typical">
	                            <a class="dropdown-toggle no-arr" href="#" >
	                              
	                                <span class="lbl">เกี่ยวกับ</span>
	                            </a>
	
	                        </div>
	                        <div class="dropdown dropdown-typical">
	                            <a  class="dropdown-toggle no-arr"href="promotion.php">
	                              
	                                <span class="lbl">โปรโมชั่น</span>
	                            </a>
	                        </div>
	
	                        <div class="dropdown dropdown-typical">
	                            <a class="dropdown-toggle no-arr" href="course.php" >
	                                
	                                <span class="lbl">คอร์ส</span>
	                            </a>
	                        </div>

	                        <div class="dropdown dropdown-typical">
	                            <a class="dropdown-toggle no-arr" href="#"  >
	                               
	                                <span class="lbl">กิจกรรม</span>
	                            </a>
	                        </div>

	                        <div class="dropdown dropdown-typical">
	                            <a class="dropdown-toggle no-arr" href="#"  >
	                                
	                                <span class="lbl">ข้อมูลโค้ช</span>
	                            </a>
	                        </div>

	                        <div class="dropdown dropdown-typical">
	                            <a class="dropdown-toggle no-arr" href="#"  >
	                               
	                                <span class="lbl">ติดต่อ</span>
	                            </a>
	                        </div>

							<div class="dropdown dropdown-typical">
	                            <a class="dropdown-toggle no-arr" href="cart.php" >
	    
	                                <span class="lbl">ตะกร้าสินค้า</span>
	                            </a>
	                        </div>


							<div class="dropdown dropdown-typical">
	                            <a class="dropdown-toggle no-arr" href="/admin/index.php" >
	    
	                                <span class="lbl">สำหรับแอดมิน</span>
	                            </a>
	                        </div>
	                        
	                       
	                        
	                    </div><!--.site-header-collapsed-in-->
	                </div><!--.site-header-collapsed-->
	            </div><!--site-header-content-in-->
	        </div><!--.site-header-content--> 
	    </div><!--.container-fluid-->
	</header><!--.site-header-->